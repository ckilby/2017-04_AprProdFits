# Example:
# for i in 343365 343366 343367; do for j in $(echo /scratch4/ckilby/ProductionOutput/Jan17_TTHbbLeptonic-02-04-23-01/GridDownload_sys/group.phys-higgs.mc15_13TeV.$i.*/*); do echo $j >> inputs_ttH_aMcP8_FS.txt; done; done

nomDir='/scratch4/ckilby/ProductionOutput/Apr17_TTHbbAnalysis-Releases-00-02-03/GridDownload'
sysDir='/scratch4/ckilby/ProductionOutput/Apr17_TTHbbAnalysis-Releases-00-02-03/GridDownload_sys'

# data
ls $nomDir/*period*/* &> inputs_data.txt



# ttH (aMcP8)
for i in 343365 343366 343367; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttH_aMcP8_FS.txt; done; done

# ttH (aMcHpp)
for i in 341177 341270 341271; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_ttH_aMcHpp_FS.txt; done; done



# tHjb (MadgraphH++)
for i in 341989 341992 341995; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphHpp_FS.txt; done; done

# tHjb (MadgraphH++ yt = +2)
for i in 341990 341993 341996; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphHpp_yt2_FS.txt; done; done

# tHjb (MadgraphH++ yt = -1)
for i in 341988 341991 341994; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphHpp_yt-1_FS.txt; done; done

# tHjb (MadgraphP8)
for i in 343267 343270 343273; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphP8_FS.txt; done; done

# tHjb (MadgraphP8 yt = +2)
for i in 343268 343271 343274; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphP8_yt2_FS.txt; done; done

# tHjb (MadgraphP8 yt = -1)
for i in 343266 343269 343272; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tHjb_MadgraphP8_yt-1_FS.txt; done; done

# tWH
for i in 341998 342001 342004; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tWH_aMcHpp_FS.txt; done; done

# tWH (yt = +2)
for i in 341999 342002 342005; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tWH_aMcHpp_yt2_FS.txt; done; done

# tWH (yt = -1)
for i in 341997 342000 342003; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tWH_aMcHpp_yt-1_FS.txt; done; done

# ttH(gammagamma) (aMcP8)
ls $nomDir/*343436*s[0-9]*/* &> inputs_ttHgamgam_aMcP8_FS.txt

# ttH(4l) (aMcH++)
ls $nomDir/*342561*s[0-9]*/* &> inputs_ttH4l_aMcHpp_FS.txt

# VBFH (PP8)
ls $nomDir/*342283*s[0-9]*/* &> inputs_VBFH_PP8_FS.txt

# WH (P8)
ls $nomDir/*342284*s[0-9]*/* &> inputs_WH_P8_FS.txt

# ZH (P8)
ls $nomDir/*342285*s[0-9]*/* &> inputs_ZH_P8_FS.txt

# bbH yb2 (aMcP8)
ls $nomDir/*342286*s[0-9]*/* &> inputs_bbHyb2_aMcP8_FS.txt

# bbH ybyt (aMcP8)
ls $nomDir/*342287*s[0-9]*/* &> inputs_bbHybyt_aMcP8_FS.txt



# ttbar (PP8 lep FS)
ls $sysDir/*410501*s[0-9]*/* &> inputs_ttbar_PP8_lep_FS.txt

# ttbar (PP8 dilep FS)
ls $sysDir/*410503*s[0-9]*/* &> inputs_ttbar_PP8_dil_FS.txt

# ttbar (PP8 lep bfil FS)
ls $sysDir/*410504*s[0-9]*/* &> inputs_ttbar_PP8_lep_bfil_FS.txt

# ttbar (PP8 dilep bfil FS)
ls $sysDir/*410505*s[0-9]*/* &> inputs_ttbar_PP8_dil_bfil_FS.txt

# ttbar (PP8 lep AFII)
ls $nomDir/*410501*a[0-9]*/* &> inputs_ttbar_PP8_lep_AFII.txt

# ttbar (PP8 dilep AFII)
ls $nomDir/*410503*a[0-9]*/* &> inputs_ttbar_PP8_dil_AFII.txt

# ttbar (PP8 lep bfil AFII)
ls $nomDir/*410504*a[0-9]*/* &> inputs_ttbar_PP8_lep_bfil_AFII.txt

# ttbar (PP8 dilep bfil AFII)
ls $nomDir/*410505*a[0-9]*/* &> inputs_ttbar_PP8_dil_bfil_AFII.txt

# ttbar (PP8up lep AFII)
ls $nomDir/*410511*a[0-9]*/* &> inputs_ttbar_PP8up_lep_AFII.txt

# ttbar (PP8down lep AFII)
ls $nomDir/*410512*a[0-9]*/* &> inputs_ttbar_PP8down_lep_AFII.txt

# ttbar (aMCP8 lep AFII)
ls $nomDir/*410225*a[0-9]*/* &> inputs_ttbar_aMcP8_lep_AFII.txt

# ttbar (aMCP8 dilep AFII)
ls $nomDir/*410226*a[0-9]*/* &> inputs_ttbar_aMcP8_dil_AFII.txt

# ttbar (aMCP8 lep bfil AFII)
ls $nomDir/*410274*a[0-9]*/* &> inputs_ttbar_aMcP8_lep_bfil_AFII.txt

# ttbar (aMCP8 dilep bfil AFII)
ls $nomDir/*410275*a[0-9]*/* &> inputs_ttbar_aMcP8_dil_bfil_AFII.txt

# ttbar (PH7 lep AFII)
ls $nomDir/*410525*a[0-9]*/* &> inputs_ttbar_PH7_lep_AFII.txt

# ttbar (PH7 dilep AFII)
ls $nomDir/*410527*a[0-9]*/* &> inputs_ttbar_PH7_dil_AFII.txt

# ttbar (PH7 lep bfil AFII)
ls $nomDir/*410528*a[0-9]*/* &> inputs_ttbar_PH7_lep_bfil_AFII.txt

# ttbar (PH7 dilep bfil AFII)
ls $nomDir/*410529*a[0-9]*/* &> inputs_ttbar_PH7_dil_bfil_AFII.txt

# ttbar (Sherpa 2.2.1 FS)
for i in 410250 410251 410252; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_ttbar_Sherpa221_FS.txt; done; done

# ttbar (PP6 FS)
for i in 410000 410009 410120 410121; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttbar_PP6_FS.txt; done; done


# diboson
for i in 361063 361064 361065 361066 361067 361068 361070 361071 361072 361073 361077 361091 361092 361093 361094 361095 361096 361097; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_diboson_FS.txt; done; done

# Wtchan (prompt) (PP FS)
for i in 410015 410016; do for j in $(echo $sysDir/*.$i.*s[0-9]*/*); do echo $j >> inputs_Wtchan_PP_dilepPrompt_FS.txt; done; done

# Wtchan (fake) (PP FS)
for i in 410013 410014; do for j in $(echo $sysDir/*.$i.*s[0-9]*/*); do echo $j >> inputs_Wtchan_PP_inclFakes_FS.txt; done; done

# Wtchan (prompt) (PP AFII)
for i in 410015 410016; do for j in $(echo $nomDir/*.$i.*a[0-9]*/*); do echo $j >> inputs_Wtchan_PP_dilepPrompt_AFII.txt; done; done

# Wtchan (prompt) (PPradHi AFII)
for i in 410103 410104; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_Wtchan_PPradHi_dilepPrompt_AFII.txt; done; done

# Wtchan (prompt) (PPradLo AFII)
for i in 410105 410106; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_Wtchan_PPradLo_dilepPrompt_AFII.txt; done; done

# Wtchan (prompt) (PH++ AFII)
for i in 410145 410146; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_Wtchan_PHpp_dilepPrompt_AFII.txt; done; done

# Wtchan (prompt) (PPDS FS)
for i in 410064 410065; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_Wtchan_PPDS_dilepPrompt_FS.txt; done; done

# schan
for i in 410025 410026; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_schan_FS.txt; done; done

# tchan
#for i in 410011 410012; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tchan_FS.txt; done; done
for i in 410011 410012; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tchan_AFII.txt; done; done

# ttW
ls $sysDir/*410155*s[0-9]*/* &> inputs_ttW_FS.txt

# ttZ
for i in 410218 410219 410220 410156 410157; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_ttZ_FS.txt; done; done

# tV
ls $sysDir/*410050*s[0-9]*/* &> inputs_tV_FS.txt

# tWW
ls $sysDir/*410081*s[0-9]*/* &> inputs_tWW_FS.txt

# tWZ
for i in 410215; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_tWZ_FS.txt; done; done

# tttt
ls $sysDir/*410080*s[0-9]*/* &> inputs_tttt_FS.txt

# Z+jets
for i in 364100 364101 364102 364103 364104 364105 364106 364107 364108 364110 364111 364112 364113 364114 364115 364116 364117 364118 364119 364120 364121 364122 364123 364124 364125 364126 364127 364128 364129 364130 364131 364132 364133 364134 364135 364136 364137 364138 364139 364140 364141 364198 364199 364200 364201 364202 364203 364204 364205 364206 364207 364208 364209 364210 364211 364212 364213 364214 364215; do for j in $(echo $sysDir/*.$i.*/*); do echo $j >> inputs_zjets_FS.txt; done; done

# W+jets
for i in 364156 364157 364158 364159 364160 364161 364162 364163 364164 364165 364166 364167 364168 364169 364170 364171 364172 364173 364174 364175 364176 364177 364178 364179 364180 364181 364182 364183 364184 364185 364186 364187 364188 364189 364190 364191 364192 364193 364194 364195 364196 364197; do for j in $(echo $nomDir/*.$i.*/*); do echo $j >> inputs_wjets_FS.txt; done; done



unset nomDir
unset sysDir
