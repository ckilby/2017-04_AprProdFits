# Config file to define a list of inputs to run through ttH-offline, with associated ttH-offline options,
# to be given to OfflineFrameworkRunner
#
# OfflineFrameworkRunner expects this config to:
#   * create a dict called inputDict
#   * fill inputDict with ProcessParams objects, with the key as the inputType of the corresponding ProcessParams
#
# Every element of the dict will be used as a single ttH-offline run

# Creating inputDict
inputDict = {}

workingDir = '/nfs/scratch4/ckilby/ttH_Work/AnalysisTop-2.4.29/2017-04_AprProdFits'

defaultConfig    = workingDir+'/offlineConfig_ICHEPlike.txt'
fakesOnlyConfig  = workingDir+'/offlineConfig_ICHEPlike_fakesOnly.txt'
promptOnlyConfig = workingDir+'/offlineConfig_ICHEPlike_promptOnly.txt'

dataConfig       = workingDir+'/offlineConfig_ICHEPlike_data.txt'

nomOnlySysts = workingDir+'/offlineConfig_ICHEPlike_systsNomOnly.txt'
allSysts     = workingDir+'/offlineConfig_ICHEPlike_systsAll.txt'

pathToInputs = workingDir+'/offFrameworkInputs/'

# # Filling inputDict with desired inputs
# inputDict['data']                     = ProcessParams(inputType = 'data',
#                                                       configFile = dataConfig,
#                                                       inputList = pathToInputs+'inputs_data.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttH_aMcP8_FS']             = ProcessParams(inputType = 'ttH_aMcP8_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttH_aMcP8_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttH_aMcHpp_FS']            = ProcessParams(inputType = 'ttH_aMcHpp_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttH_aMcHpp_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# ####################################################################################################################################

inputDict['tHjb_MadgraphHpp_FS']      = ProcessParams(inputType = 'tHjb_MadgraphHpp_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphHpp_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tHjb_MadgraphHpp_yt2_FS']   = ProcessParams(inputType = 'tHjb_MadgraphHpp_yt2_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphHpp_yt2_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tHjb_MadgraphHpp_yt-1_FS']  = ProcessParams(inputType = 'tHjb_MadgraphHpp_yt-1_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphHpp_yt-1_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tHjb_MadgraphP8_FS']      = ProcessParams(inputType = 'tHjb_MadgraphP8_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphP8_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tHjb_MadgraphP8_yt2_FS']   = ProcessParams(inputType = 'tHjb_MadgraphP8_yt2_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphP8_yt2_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tHjb_MadgraphP8_yt-1_FS']  = ProcessParams(inputType = 'tHjb_MadgraphP8_yt-1_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tHjb_MadgraphP8_yt-1_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tWH_aMcHpp_FS']      = ProcessParams(inputType = 'tWH_aMcHpp_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tWH_aMcHpp_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tWH_aMcHpp_yt2_FS']   = ProcessParams(inputType = 'tWH_aMcHpp_yt2_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tWH_aMcHpp_yt2_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

inputDict['tWH_aMcHpp_yt-1_FS']  = ProcessParams(inputType = 'tWH_aMcHpp_yt-1_FS',
                                                      configFile = defaultConfig,
                                                      inputList = pathToInputs+'inputs_tWH_aMcHpp_yt-1_FS.txt',
                                                      systFile = allSysts,
                                                      extraOptionsDict = {})

# inputDict['ttHgamgam_FS']             = ProcessParams(inputType = 'ttHgamgam_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttHgamgam_aMcP8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttH4l_FS']                 = ProcessParams(inputType = 'ttH4l_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttH4l_aMcHpp_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['VBFH_FS']                  = ProcessParams(inputType = 'VBFH_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_VBFH_PP8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['WH_FS']                    = ProcessParams(inputType = 'WH_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_WH_P8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ZH_FS']                    = ProcessParams(inputType = 'ZH_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ZH_P8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['bbHyb2_FS']                = ProcessParams(inputType = 'bbHyb2_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_bbHyb2_aMcP8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['bbHybyt_FS']               = ProcessParams(inputType = 'bbHybyt_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_bbHybyt_aMcP8_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

####################################################################################################################################

# inputDict['ttbar_PP8_lep_fakes_FS']   = ProcessParams(inputType = 'ttbar_PP8_lep_fakes_FS',
#                                                       configFile = defaultConfig,
#                                                       #configFile = fakesOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
# # # For combination overlap checks
# # inputDict['ttbar_PP8_lep_FS']         = ProcessParams(inputType = 'ttbar_PP8_lep_FS',
# #                                                       configFile = defaultConfig,
# #                                                       #configFile = fakesOnlyConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_FS.txt',
# #                                                       systFile = allSysts,
# #                                                       extraOptionsDict = {})

# # inputDict['ttbar_PP8_lep_bfil_fakes_FS']    = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_fakes_FS',
# #                                                       configFile = defaultConfig,
# #                                                       #configFile = fakesOnlyConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_FS.txt',
# #                                                       systFile = allSysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_lep_bfil_fakes_AFII'] = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_fakes_AFII',
#                                                       configFile = defaultConfig,
#                                                       #configFile = fakesOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_FS']         = ProcessParams(inputType = 'ttbar_PP8_dil_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_bfil_FS']    = ProcessParams(inputType = 'ttbar_PP8_dil_bfil_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_bfil_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_lep_AFII']       = ProcessParams(inputType = 'ttbar_PP8_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_PP8_lep_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PP8_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PP8_lep_bfil_AFII.txt',
# #                                                       #systFile = allSysts_forAFII,
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_AFII']       = ProcessParams(inputType = 'ttbar_PP8_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PP8_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8_dil_bfil_AFII.txt',
#                                                       #systFile = allSysts_forAFII,
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

####################################################################################################################################

# inputDict['ttbar_PP8up_lep_AFII']     = ProcessParams(inputType = 'ttbar_PP8up_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8up_lep_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP8down_lep_AFII']   = ProcessParams(inputType = 'ttbar_PP8down_lep_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP8down_lep_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_PH7_lep_AFII']       = ProcessParams(inputType = 'ttbar_PH7_lep_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PH7_lep_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# # inputDict['ttbar_PH7_lep_bfil_AFII'] = ProcessParams(inputType = 'ttbar_PH7_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_PH7_lep_bfil_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_PH7_dil_AFII']       = ProcessParams(inputType = 'ttbar_PH7_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PH7_dil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PH7_dil_bfil_AFII']  = ProcessParams(inputType = 'ttbar_PH7_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PH7_dil_bfil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# # inputDict['ttbar_aMcP8_lep_AFII']     = ProcessParams(inputType = 'ttbar_aMcP8_lep_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# # inputDict['ttbar_aMcP8_lep_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_lep_bfil_AFII',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_lep_bfil_AFII.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       extraOptionsDict = {})

# inputDict['ttbar_aMcP8_dil_AFII']     = ProcessParams(inputType = 'ttbar_aMcP8_dil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_aMcP8_dil_bfil_AFII']= ProcessParams(inputType = 'ttbar_aMcP8_dil_bfil_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_aMcP8_dil_bfil_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_Sherpa221_FS']       = ProcessParams(inputType = 'ttbar_Sherpa221_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_Sherpa221_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['ttbar_PP6_FS']             = ProcessParams(inputType = 'ttbar_PP6_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttbar_PP6_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {'DoTTbarBfilter':'true',' DoTTbarDileptonFilter':'true'})


                                      
# inputDict['diboson_FS']               = ProcessParams(inputType = 'diboson_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_diboson_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['Wtprompt_PP_FS']           = ProcessParams(inputType = 'Wtprompt_PP_FS',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PP_dilepPrompt_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['Wtfakes_PP_FS']            = ProcessParams(inputType = 'Wtfakes_PP_FS',
#                                                       #configFile = defaultConfig,
#                                                       configFile = fakesOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PP_inclFakes_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})

# inputDict['Wtprompt_PP_AFII']         = ProcessParams(inputType = 'Wtprompt_PP_AFII',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PP_dilepPrompt_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['Wtprompt_PPradHi_AFII']    = ProcessParams(inputType = 'Wtprompt_PPradHi_FS',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PPradHi_dilepPrompt_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['Wtprompt_PPradLo_AFII']    = ProcessParams(inputType = 'Wtprompt_PPradLo_FS',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PPradLo_dilepPrompt_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['Wtprompt_PHpp_AFII']      = ProcessParams(inputType = 'Wtprompt_PHpp_AFII',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PHpp_dilepPrompt_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})

# inputDict['Wtprompt_PPDS_FS']        = ProcessParams(inputType = 'Wtprompt_PPDS_FS',
#                                                       #configFile = defaultConfig,
#                                                       configFile = promptOnlyConfig,
#                                                       inputList = pathToInputs+'inputs_Wtchan_PPDS_dilepPrompt_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       extraOptionsDict = {})
                                      
# # inputDict['schan_FS']                 = ProcessParams(inputType = 'schan_FS',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_schan_FS.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       #systFile = allSysts,
# #                                                       extraOptionsDict = {})

# inputDict['schan_AFII']               = ProcessParams(inputType = 'schan_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_schan_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       #systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# # inputDict['tchan_FS']                 = ProcessParams(inputType = 'tchan_FS',
# #                                                       configFile = defaultConfig,
# #                                                       inputList = pathToInputs+'inputs_tchan_FS.txt',
# #                                                       systFile = nomOnlySysts,
# #                                                       #systFile = allSysts,
# #                                                       extraOptionsDict = {})

# inputDict['tchan_AFII']               = ProcessParams(inputType = 'tchan_AFII',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tchan_AFII.txt',
#                                                       systFile = nomOnlySysts,
#                                                       #systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['ttZ_FS']                   = ProcessParams(inputType = 'ttZ_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttZ_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['ttW_FS']                   = ProcessParams(inputType = 'ttW_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_ttW_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['4top_FS']                  = ProcessParams(inputType = '4top_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tttt_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['tV_FS']                    = ProcessParams(inputType = 'tV_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tV_FS.txt',
#                                                       systFile = allSysts,
#                                                      extraOptionsDict = {})
                                      
# inputDict['tWZ_FS']                   = ProcessParams(inputType = 'tWZ_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tWZ_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['tWW_FS']                   = ProcessParams(inputType = 'tWW_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_tWW_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['zjets_FS']                 = ProcessParams(inputType = 'zjets_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_zjets_FS.txt',
#                                                       systFile = allSysts,
#                                                       extraOptionsDict = {})
                                      
# inputDict['wjets_FS']                 = ProcessParams(inputType = 'wjets_FS',
#                                                       configFile = defaultConfig,
#                                                       inputList = pathToInputs+'inputs_wjets_FS.txt',
#                                                       systFile = nomOnlySysts,
#                                                       #systFile = allSysts,
#                                                       extraOptionsDict = {})
